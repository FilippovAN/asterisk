FROM rockylinux/rockylinux:9.1

# builds argument
ARG AST_VERSION=20
ARG SRTP_VERSION=1.5.4
ARG SOURCE_URL=https://gitlab.com/FilippovAN/asterisk/-/raw/main/

WORKDIR /usr/local/src/
WORKDIR /usr/local/src/

RUN curl "${SOURCE_URL}asterisk-${AST_VERSION}.tar.gz" --output "asterisk.tar.gz"
RUN curl "${SOURCE_URL}libsrtp-${SRTP_VERSION}.tar.gz" --output "libsrtp.tar.gz"


# preinstall before install asterisk packages
RUN dnf update -y && \
    dnf install -y epel-release && \
    dnf install -y freetds-devel freetds-libs freetds && \
    dnf install -y samba samba-client && \
    dnf install -y make mc htop vim net-tools telnet traceroute mlocate zip gzip bzip2


# install asterisk packages
RUN dnf install -y kernel-devel mc man wget gcc ncurses ncurses-devel patch mtr rsync iptraf unixODBC gcc-c++ libxml2 libxml2-devel sqlite sqlite-devel libtool-ltdl libcurl-devel net-snmp net-snmp-devel unzip rsync libuuid-devel libedit-devel && \
    dnf --enablerepo=crb install -y unixODBC-devel libsrtp-devel jansson-devel libtool-ltdl-devel

# install librtp
RUN tar xf "/usr/local/src/libsrtp.tar.gz" && \
    cd /usr/local/src/libsrtp-* && ./configure && make && make install

# install asterisk
RUN tar xf "/usr/local/src/asterisk.tar.gz" && \
    cd /usr/local/src/asterisk-* && \
    make clean && \
    rm -rf menuselect.makeopts && \
    ./configure && \
    make menuselect && \
    menuselect/menuselect --enable app_macro menuselect.makeopts && \
    make && \
    make install && \
    make samples

# SET Timezone
ENV TZ="Europe/Kiev"

# clear cache dnf and others files
RUN dnf clean all && \
    rm -rf /var/cache/dnf && \ 
    rm -rf /usr/local/src/*

WORKDIR /etc/asterisk

CMD ["asterisk", "-cvvvv"]
